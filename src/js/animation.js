~ function () {
	'use strict';
	var $ = TweenMax;

	window.init = function () {
		play();

		
	}

	function play() {

		MorphSVGPlugin.convertToPath('circle');
		MorphSVGPlugin.convertToPath('polygon');

		var tl = new TimelineLite();

		tl.to('#circle', 1, {morphSVG:'#butterfly',fill:'#ED919A'}, "+=1")
		tl.to('#circle', 1, {morphSVG:'#owl',fill:'#898989'}, "+=1")
		tl.to('#circle', 1, {morphSVG:'#girl',fill:'#D1D1D1'}, "+=1")
		
	}
	

}();

